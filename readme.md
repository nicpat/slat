# Simple Laravel Authentication Tokens

![Packagist Version](https://img.shields.io/packagist/v/appnic/slat.svg)

A simple token system for authenticating users in Laravel.

## What is this?

This is a package that adds a simple token authentication system to Laravel. You can create multiple tokens per user, 
authenticate incoming requests with the included authentication guard and invalidate tokens when a user logs out.

The way how tokens are managed is configurable. By default, all tokens are stored in the database (via the database 
driver). However, you can implement your own way how to store and validate your own tokens. 

## Installation

It is recommended to use composer to manage the dependencies in your project. 

Install the package with this command:

`composer require appnic/slat`

The service provider of the package will automatically be detected.

## Publish the configuation

You can publish the configuration with the following command:

`php artisan vendor:publish --provider="appnic\slat\Providers\SlatProvider" --tag="config"`

## Set up your model

Add the `appnic\slat\Contracts\TokenOwner` interface and the `appnic\slat\Traits\HasTokens` trait to the model which 
should own the tokens (most likely the default `App\User` model):

```php
<?php

namespace App;

use appnic\slat\Contracts\TokenOwner;
use appnic\slat\Traits\HasTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements TokenOwner {
    use HasTokens;

    // The rest of your model
}
```

**IMPORTANT:** If your user model has another namespace or name than the default Laravel user model (`\App\User`) 
you'll need to specify this in the `slat.php` config file under `drivers` -> `database` -> `owner_model` and in your 
user provider configuration in the `auth.php` config file.

## Guard config

By configuring which auth guard to use, you tell your Laravel installation how to authenticate each incoming request. 
Each guard has a guard driver which contains the logic to authenticate the request. SLAT uses its own `slat` guard 
driver to handle this.

To actually use the `slat` guard driver, you can either add a new guard or modify an existing one. Guards are defined 
in your `config\auth.php` file.

A common use case for SLAT is to authenticate API requests. If this is the case in your project, you will most likely 
want to modify the existing `api` guard: Simply change the value of `driver` to `slat`:

```php
<?php

return [

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'slat', // <-- replace "token" with "slat"
            'provider' => 'users',
            'hash' => false, // <-- you can delete this line
        ],
    ],

    ...
```

Also, we need to tell Laravel which guard to actually use. If your application uses SLAT as the only way to check for 
authentication, you can simply change the `guard` key of the `defaults` array to the guard name of your SLAT guard:

```php
return [
    'defaults' => [
        'guard' => 'api' // <-- add the name of your SLAT guard
        'passwords' => 'users'
    ]

    ...
``` 

Some applications however use a combination of SLAT and session authentication. In this case, you need to tell Laravel 
when to use which guard. You can do this by specifying the guard in each call of the `Auth` facade:

```php
use Illuminate\Support\Facades\Auth;

Auth::guard('your_guard_name')->user();
```

However, this adds code clutter and increases chances for introducing bugs. Therefore, we recommend to use the `guard` 
middleware included in this package: 

- register the `guard` middleware to the `routeMiddleware` array in your `app/Http/Kernel.php` file 
(`'guard' => \appnic\slat\Middleware\Guard::class`)
- use the `guard` middleware in your routes using the desired guard name as the parameter 
(e.g. `Route::middleware("guard:your_guard_name")`)

## Setting up your token storage

SLAT uses drivers to store user tokens. By default the `database` driver is used.

### Using the database driver

To use the default database driver to store your tokens, simply publish the migration containing the tokens table:

`php artisan vendor:publish --provider="appnic\slat\Providers\SlatProvider" --tag="migrations"`

If you want to change the table name of the token table, check out the config file, which contains a block with config 
options for the database driver.

### Using another driver

If you need to store your tokens at another place than your database, you can write your own driver.

- Create a class implementing `appnic\slat\Contracts\TokenManager`. Check out 
`appnic\slat\Drivers\Database\DatabaseTokenManager` if you need an example
- Create a class implementing `appnic\slat\Contracts\Token`. This is your token class. Check out the DatabaseToken 
class if you need an example
- In your SLAT configuration, add a new entry to the `drivers` array. You need to at least provide the `manager` key, 
pointing to your `TokenManager` implementation. The current driver and its configuration will be bound to a new 
`SlatConfig` object which can be `resolve`d from Laravels service container. 

## Usage

Resolve the token manager from the service container to do any token modifications:
```php
use appnic\slat\Contracts\TokenManager;

$tokenManager = resolve(TokenManager::class);
```

Create a new token for a user:

```php
use \App\User;

$user = User::first();
$tokenManager->create($user, 'optional-token-name');
```

Mark a token as expired: 

```php
use \App\User;

$user = User::first();
$tokenManager->expire($user->tokens->first());
```

Delete a token:
```php
use \App\User;

$user = User::first();
$tokenManager->delete($user->tokens->first());
```

Get a token object from a token string:
```php
$tokenManager->fromSecret($tokenString);
```

Check if there is a token with a given token string:
```php
$tokenManager->exists($tokenString);
```

## Sending requests to SLAT guarded applications
SLAT checks the `Authentication` header field of an HTTP package for authentication tokens. It uses a typical "Bearer 
authentication" format, i.e. the contents of the `Authentication` header field should be formatted like this: 
`Bearer <your-slat-token>`

If you are using Javascript to consume your API, you could do something like this:
```javascript
let bearerToken = 'your-slat-token';

const axios = require('axios');
axios.defaults.baseURL = 'http://your-api-server.example.com/';
axios.defaults.headers.common = {'Authorization': 'Bearer '+bearerToken};
```