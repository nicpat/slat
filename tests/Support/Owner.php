<?php


namespace appnic\slat\Tests\Support;

use appnic\slat\Contracts\TokenOwner;
use appnic\slat\Traits\HasTokens;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class Owner extends Model implements TokenOwner
{
    use HasTokens, Authenticatable;

    protected $fillable = ['name', 'password'];
}