<?php

namespace appnic\slat\Tests\Unit;

use appnic\slat\Contracts\TokenManager;
use appnic\slat\Slat;
use appnic\slat\Tests\TestCase;

class SlatTest extends TestCase
{
    public function testFacadeBinding() {
        $this->assertInstanceOf(TokenManager::class, Slat::getFacadeRoot());
    }
}