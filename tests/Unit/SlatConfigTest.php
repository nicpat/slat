<?php

namespace appnic\slat\Tests\Unit;

use appnic\slat\Contracts\Token;
use appnic\slat\Contracts\TokenManager;
use appnic\slat\Drivers\Database\DatabaseTokenManager;
use appnic\slat\Providers\SlatProvider;
use appnic\slat\SlatConfig;
use appnic\slat\SlatGuard;
use appnic\slat\Tests\Support\Owner;
use appnic\slat\Tests\TestCase;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use InvalidArgumentException;

class SlatConfigTest extends TestCase
{
    /** @var SlatConfig */
    public $config;

    public function getEnvironmentSetUp($app)
    {
        parent::getEnvironmentSetUp($app);

        $this->config = (new SlatConfig())
            ->driver('dummyDriver', DatabaseTokenManager::class, [
                'param1' => 'paramvalue1',
                'param2' => 'paramvalue2'
            ]);
    }

    public function testInvalidManagerClass() {
        $this->expectException(InvalidArgumentException::class);

        // Set the driver with a class that does not implement TokenManager. Must throw an exception
        (new SlatConfig())->driver(
            'dummy',
            'Not\Existing\Class',
            []);
    }

    public function testGetDriverName() {
        $this->assertSame('dummyDriver', $this->config->getDriverName());
    }

    public function testGetDriverManager() {
        $this->assertSame(DatabaseTokenManager::class, $this->config->getDriverManager());
    }

    public function testGetDriverOptions() {
        $this->assertSame([
            'param1' => 'paramvalue1',
            'param2' => 'paramvalue2'
        ], $this->config->getDriverOptions());
    }

    public function testGetDriverOption() {
        $this->assertSame('paramvalue1', $this->config->getDriverOption('param1'));
        $this->assertSame('paramvalue2', $this->config->getDriverOption('param2', 'defaultval'));
        $this->assertSame('defaultval', $this->config->getDriverOption('doesntexist', 'defaultval'));
    }
}