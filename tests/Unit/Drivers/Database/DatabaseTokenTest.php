<?php

namespace appnic\slat\Tests\Unit\Drivers\Database;

use appnic\slat\Contracts\Token;
use appnic\slat\Contracts\TokenManager;
use appnic\slat\Drivers\Database\DatabaseTokenManager;
use appnic\slat\Tests\Support\Owner;
use appnic\slat\Tests\TestCase;
use Illuminate\Support\Facades\Hash;

class DatabaseTokenTest extends TestCase
{
    /** @var Token */
    public $token;

    /** @var TokenManager */
    public $tokenManager;

    public function setUp() : void
    {
        parent::setUp();

        $owner = Owner::create(['name'=>'testowner', 'password'=>Hash::make('testpassword')]);

        $this->tokenManager = new DatabaseTokenManager();
        $this->token = $this->tokenManager->create($owner, 'tokenname');
    }

    public function testAccessed() {
        $this->assertSame(0, $this->token->getAccessCount());
        $this->assertNull($this->token->getLastAccessDate());

        $this->token->accessed();

        $this->assertSame(1, $this->token->getAccessCount());
        $this->assertNotNull($this->token->getLastAccessDate());
    }
}