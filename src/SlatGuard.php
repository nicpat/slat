<?php

namespace appnic\slat;

use appnic\slat\Contracts\TokenManager;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Http\Request;

class SlatGuard implements Guard
{
    use GuardHelpers;

    protected $request;

    protected $manager;

    public function __construct(Request $request, UserProvider $provider, TokenManager $manager) {
        $this->provider = $provider;
        $this->request = $request;
        $this->manager = $manager;
    }

    /**
     * Get the currently authenticated user.
     *
     * @return Authenticatable|null
     */
    public function user() : ?Authenticatable
    {
        if(!is_null($this->user)) {
            return $this->user;
        }

        $token = $this->manager->fromSecret($this->request->bearerToken());

        if(is_null($token)) {
            return null;
        }

        return $token->getOwner();
    }

    public function validate(array $credentials = []) : bool
    {
        $user = $this->provider->retrieveByCredentials($credentials);
        return $this->hasValidCredentials($user, $credentials);
    }

    protected function hasValidCredentials($user, $credentials)
    {
        return !is_null($user) && $this->provider->validateCredentials($user, $credentials);
    }
}
