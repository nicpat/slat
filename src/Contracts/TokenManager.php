<?php

namespace appnic\slat\Contracts;

use appnic\slat\Collections\TokenCollection;
use Illuminate\Support\Carbon;

interface TokenManager
{
    /**
     * Creates a new Token for the given TokenOwner with an optional token name
     *
     * @param TokenOwner $owner
     * @param string|null $name
     * @return Token
     */
    public function create(TokenOwner $owner, string $name = null) : Token;

    /**
     * Marks the token as expired immediately or at the given date.
     *
     * @param Token $token
     * @param Carbon|null $at
     * @return TokenManager
     */
    public function expire(Token $token, Carbon $at = null) : TokenManager;

    /**
     * Deletes the given token.
     *
     * @param Token $token
     * @return TokenManager
     */
    public function delete(Token $token) : TokenManager;

    /**
     * Returns a collection of tokens. Filtered by TokenOwner if one is given
     *
     * @param TokenOwner|null $owner
     * @return TokenCollection
     */
    public function tokens(TokenOwner $owner = null) : TokenCollection;

    /**
     * Returns if a token with the given secret exists
     *
     * @param string $secret
     * @return Token|null
     */
    public function fromSecret(string $secret) : ?Token;

    /**
     * Returns a token or null for the given secret
     *
     * @param string $secret
     * @return bool
     */
    public function exists(string $secret) : bool;
}