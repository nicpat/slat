<?php

namespace appnic\slat\Contracts;


use Illuminate\Support\Carbon;

interface Token
{
    /**
     * Returns the secret of the token
     *
     * @return String
     */
    public function getSecret() : String;

    /**
     * Returns the name of the token
     * @return String|null
     */
    public function getName() : ?String;

    /**
     * Returns the owner of the token
     *
     * @return TokenOwner
     */
    public function getOwner() : TokenOwner;

    /**
     * Returns the date when the token was last accessed (defined by calling the method accessed())
     *
     * @return Carbon|null
     */
    public function getLastAccessDate() : ?Carbon;

    /**
     * Returns how many times this token was accessed (defined by calling the method accessed())
     *
     * @return int
     */
    public function getAccessCount() : int;

    /**
     * Returns when the token expired or will expire
     *
     * @return Carbon|null
     */
    public function getExpireDate() : ?Carbon;

    /**
     * Returns if the token has expired
     *
     * @return bool
     */
    public function hasExpired() : bool;

    /**
     * Increases the access counter and sets the last access date to now
     *
     * @return Token
     */
    public function accessed() : Token;
}