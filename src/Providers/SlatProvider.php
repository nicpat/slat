<?php

namespace appnic\slat\Providers;

use appnic\slat\Contracts\TokenManager;
use appnic\slat\Drivers\Database\DatabaseTokenManager;
use appnic\slat\SlatConfig;
use appnic\slat\SlatGuard;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class SlatProvider extends ServiceProvider
{
    public function boot(Filesystem $filesystem) {
        $this->publishFiles($filesystem);

        if(!$this->app->bound(SlatConfig::class)) {
            $this->bindConfigFromFile();
        }

        $config = resolve(SlatConfig::class);

        $managerImplementation = $config->getDriverManager();
        $manager = new $managerImplementation();

        $this->app->bind(TokenManager::class, $managerImplementation);

        $this->addSlatGuardDriver($manager);
    }

    /**
     * Register bindings
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/slat.php', 'slat');
    }

    public function publishFiles(Filesystem $filesystem) {
        $this->publishes([
            __DIR__.'/../../config/slat.php' => config_path('slat.php')
        ], 'config');

        $this->publishes([
            __DIR__.'/../../database/migrations/create_tokens_table.php.stub' => $this->getMigrationFileName($filesystem)
        ], 'migrations');
    }

    public function bindConfigFromFile() {
        $driverName = config('slat.driver');
        $driver = config('slat.drivers.' . $driverName);

        $this->app->instance(
            SlatConfig::class,
            (new SlatConfig())->driver($driverName, $driver['manager'], Arr::except($driver, ['manager']))
        );
    }

    /**
     * Add the guard to the array of guards
     */
    public function addSlatGuardDriver(TokenManager $manager) {
        $this->app['auth']->extend('slat', function($app, $name, array $config) use ($manager) {
            return new SlatGuard(
                $app['request'],
                $app['auth']->createUserProvider($config['provider']),
                $manager
            );
        });
    }

    /**
     * Returns the name of an existing migration file if found or the name of the new file
     *
     * @param Filesystem $filesystem
     * @return string
     */
    public function getMigrationFileName(Filesystem $filesystem) : string {
        $timestamp = date('Y_m_d_His');

        return Collection::make($this->app->databasePath().DIRECTORY_SEPARATOR.'migrations'.DIRECTORY_SEPARATOR)
            ->flatMap(function($path) use ($filesystem) {
                return $filesystem->glob($path.'*_create_tokens_table.php');
            })->push($this->app->databasePath().'/migrations/'.$timestamp.'_create_tokens_table.php')->first();
    }
}